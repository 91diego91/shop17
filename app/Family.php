<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    protected $fillable = ['code', 'name'];//para que rellene solo estos campos cuando creo un registro nuevo

    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
