<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;//cambiar a true para que nos deje hacer cosas
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //para no poner las validaciones en el controlador
        return [
        'code' => 'required|max:4',
        'name' => 'required|max:40',
        'price' => 'numeric',
        'family_id' => 'exists:families,id',
        ];
    }
}
