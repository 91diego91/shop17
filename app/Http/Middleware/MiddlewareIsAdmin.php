<?php

namespace App\Http\Middleware;

use Closure;

class MiddlewareIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user && $user->email === 'diegopalomera4@gmail.com') {
            return $next($request);
        }
        abort(404);
    }
}
