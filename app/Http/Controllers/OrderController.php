<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Product;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        //para que si entro con usuario cliente solo me enseñe sus pedidos y si soy admin todos
        $user = $request->user();
        if (is_null($user) || $user->isClient()) {
            $orders = Order::where('user_id', $user->id)->paginate();
        } else {
            $orders = Order::paginate();
        }
        return view('order.index', ['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = session('products');
        if (is_null($products)) {
            $products = array();
        }
        return view('order.create', ['products' => $products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order;
        $user = $request->user();
        $order->user_id = $user->id;
        $order->save();
        //aqui ya lo tengo guardado

        $products = session('products');
        foreach ($products as $product) {
            $order->products()->attach($product['id'], [
                'price' => $product['price'],
                'quantity' => $product['quantity'],
                ]);
        }
        session()->forget('products');//borra el array products de la sesion,una vez que lo tenemos hecho el pedido
        return redirect('/orders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        $this->authorize('view', $order);
        return view('order.show', ['order' => $order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //para añadir a la compra y que se quede en sesion
    public function addProduct($id, Request $request)
    {
        // \Session::forget('order');
        $product = Product::findOrFail($id);
        $products = session('products');
        $element['id'] = $id;
        $element['quantity'] = 1;
        $element['price'] = $product->price;
        $element['product'] = $product;

        $products[] = $element;
        \Session::put('products', $products);

        return redirect('orders/create');
    }
}
