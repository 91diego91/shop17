<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PruebasController extends Controller//esta clase es para pruebas
{
    public function index($method)
    {
        $this->$method();
    }

    public function products()//llamo al metodo products desde el navegador para ver que me coge todo
    {
        //toda la tabla
        echo DB::table('products')->toSql();//me muestra la consulta que hago
        //$result = DB::table('products')->get();
        $result = DB::table('products')->paginate();//para que salga las paginas
        dd($result);
    }

    public function users()
    {
        //toda la tabla
        echo DB::table('users')->toSql();
        $result = DB::table('users')->get();
        dd($result);
    }

    public function product()
    {
        
        echo DB::table('products')->toSql();
        $result = DB::table('products')->first();
        dd($result);
    }

    public function user1()
    {
        
        echo DB::table('users')->select('email', 'name')->toSql();
        $result = DB::table('users')->select('email', 'name')->first();
        dd($result);
    }

    public function families1()
    {
        
        echo DB::table('families')->select('code')->toSql();
        $result = DB::table('families')->select('code')->get();
        dd($result);
    }

    public function families2()
    {
        
        echo DB::table('families')->select('code')->where('code', 'like', 'M%')->toSql();
        $result = DB::table('families')->select('code')->where('code', '=', 'PERI')->get();
        dd($result);
    }

    public function families3()
    {
        
        echo DB::table('families')->select('code')->where('code', 'like', 'M%')->toSql();
        $result = DB::table('families')->select('code')->where('code', 'like', 'M%')->where('code', 'like', '%C')->get();
        dd($result);
    }

    public function families4()//ordenar
    {
        
        echo DB::table('families')->select('code')->where('code', 'like', 'M%')->orderBy('code')->toSql();
        $result = DB::table('families')->select('code')->where('code', 'like', 'M%')->orderBy('code')->get();
        dd($result);
    }

    public function families5()//ordenar decreciente
    {
        
        echo DB::table('families')->select('code')->where('code', 'like', 'M%')->orderBy('code', 'desc')->toSql();
        $result = DB::table('families')->select('code')->where('code', 'like', 'M%')->orderBy('code', 'desc')->get();
        dd($result);
    }

    public function join1()//producto cartesiano
    {
        
        echo DB::table('families')->crossJoin('products')->toSql();
        $result = DB::table('families')->join('products')->get();
        dd($result);
    }

    public function join2()//producto cartesiano
    {
        
        echo DB::table('families')->join('products', 'families.id', '=', 'family_id')->toSql();
        $result = DB::table('families')->join('products', 'families.id', '=', 'family_id')->select('families.name', 'products.name as product')->get();
        dd($result);
    }

    public function sql()
    {
        
        echo DB::raw("SELECT * FROM products WHERE family_id = 3");
        $result = DB::select("SELECT * FROM products WHERE family_id = 3");
        dd($result);
    }
}
