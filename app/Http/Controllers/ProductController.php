<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//añadir el modelo
use App\Product;
use App\Family;

//validaciones
use App\Http\Request\ProductFormRequest;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //esto para aplicar el middle y que no me deje entrar en la direccion si no estoy autenticado(en el product test)
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        //implementando la politica
        $product = new Product();
        $user = $request->user();
        if ($user->cannot('view', $product)) {
            return redirect('/home');
        }

        $products = Product::paginate();
        //dd($products);
        return view('product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Product::class);
        $families = Family::all();
        return view('product.create', ['families' => $families]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    //atento para al request por la validacion
    public function store(ProductFormRequest $request)
    {
        $this->authorize('create', Product::class);
        $this->validate($request, [
            'code' => 'required|max:4|unique:products,code',
        ]);

        $product = new Product($request->all());
        $product->save();
        $id = $product->id;
        return redirect('/products/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $this->authorize('view', $product);
        return view('product.show', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $this->authorize('update', $product);
        $families = Family::all();//esto para ver una lista de todas las familias por si lo quiero cambiar de familia
        return view('product.edit', ['product' => $product, 'families' => $families]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validación: 2 modos.
        //1. Validamos aquí en el controlador. Descomentar lo de abajo
        //2. Valida en el ProductFormRequest.
        //   La clase está creada con el código.
        //   La clase se incluye en la cabecera: use App\...
        //   Se inyecta el objeto request en este método update.
        // $this->validate($request, [
        // 'code' => 'required|max:4|unique:products,code,' . $id,
        // 'name' => 'required|max:40',
        // ]);
        $this->validate($request, [
            'code' => 'required|max:4|unique:products,code,' . $id,
        ]);

        $product = Product::findOrFail($id);
        $this->authorize('update', $product);
        $product->code = $request->code;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->family_id = $request->family_id;
        $product->save();
        
        return redirect('/products');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $this->authorize('delete', $product);
        if ($product->orders()->count()) {
            return 'Fallo de borrado. Está incluido en pedidos.';
        }
        Product::destroy($id);
        return redirect('/products');
    }
}
