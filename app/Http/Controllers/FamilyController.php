<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Family;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $family = new Family();

        //lo de las politicas
        $user = $request->user();
        if (! $user->can('view', $family)) {
            return redirect('/home');
        }

        //$families = Family::all();
        $families = Family::paginate(10);//cuatro registros por pagina
        //return $families;
        if ($request->ajax()) {
            return $families;
        } else {
            return view('family.index', ['families' => $families]);//que nos devuelva la lista, pero hay que decirle que le pasamos
        }
    }

    public function ajax()
    {
        return view('family.ajax');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Family::class);//lo de la politicas, le paso el nombre de la politica(regla) y el modelo al que se le aplica
        return view('family.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [//para validar
          'code' => 'required|unique:families|max:4',
          'name' => 'required|max:40',
        ]);

        $family = new Family($request->all());
        $family->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/families');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $family = Family::findOrFail($id);//busca en el registro con el id que le paso
        $this->authorize('view', $family);//lo de los roles, aqui en vez de pasarle un modelo como en el create,l le estoy pasando ya una variable la cual es un objeto de family
        if ($request->ajax()) {
            return $family;
        } else {
            return view('family.show', ['family' => $family]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $family = Family::findOrFail($id);
        $this->authorize('update', $family);
        return view('family.edit', ['family' => $family]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [//para validar
          'code' => 'required|max:4|unique:families,id,' . $id,//para que si yo modifico y el codigo no lo toco no me de problemas diciendo que esta duplicado
          'name' => 'required|max:40',
        ]);

        $family = Family::findOrFail($id);
        $family->code = $request->code;
        $family->name = $request->name;
        $family->save();

        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/families');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Family::find($id)->delete();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/families');
        }
    }

    public function showProducts($id, Request $request)
    {
        $family = Family::findOrFail($id);//busca en el registro con el id que le paso
        $products = $family->products;
        if ($request->ajax()) {
            return $products;
        } else {
            return view('family.products', ['family' => $family, 'products' => $products]);
        }
    }
}
