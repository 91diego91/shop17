<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->visit('/products')
            ->seePageIs('/login');//cuando vas a home, te lleva a login si no estas logeado
        
        $this->visit('/products')
            ->seePageIs('/products')//cuando te logeas te lleva a home
            ->see('Lista de Productos');
    }
}
