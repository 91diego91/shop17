@extends('layouts.app')

@section('title', 'Index Family')

@section('content')
    <h1>Lista de Familias</h1>
    <p><a href="/families/create">Nuevo</a></p>

    <table class="table table-striped">
        <tr>
            <th>Id</th>
            <th>Code</th>
            <th>Name</th>
            <th>Acciones</th>
        </tr>
        @foreach ($families as $family)
        <tr>
            <td>{{ $family['id'] }}</td>
            <td>{{ $family['code'] }}</td>
            <td>{{ $family['name'] }}</td>
            <td>
                <form method="post" action="/families/{{ $family->id}}">
                {{ csrf_field() }}

                    @can('update', $family)
                        <a href="/families/{{ $family['id'] }}/edit">Editar</a>
                    @endcan

                    <input type="hidden" name="_method" value="DELETE">
                    @can('delete', $family)
                        <input type="submit" value="Borrar">
                    @endcan
                    
                    <a href="/families/{{ $family->id }}">Ver</a>
                </form>
            </td>
        </tr>  
        @endforeach
    </table>
    {!! $families->render() !!}
@endsection
