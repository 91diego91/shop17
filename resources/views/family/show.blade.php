@extends('layouts.app')

@section('title', 'Index Family')

@section('content')
    <h1>Detalle de familia</h1>
    <p>Id: {{ $family['id'] }}</p>
    <p>Codigo: {{ $family['code'] }}</p>
    <p>Nombre: {{ $family['name'] }}</p>
@endsection
