@extends('layouts.app')


@section('content')
    <h1>Detalle de familia</h1>
    <p>Id: {{ $family['id'] }}</p>
    <p>Codigo: {{ $family['code'] }}</p>
    <p>Nombre: {{ $family['name'] }}</p>

    <h1>Lista de Productos</h1>

       <table class="table table-striped">
        <tr>
            <th>Id</th>
            <th>Code</th>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Familia</th>
            <th>Acciones</th>
        </tr>
      @foreach ($products as $product)
        <tr>
            <td>{{ $product['id'] }}</td>
            <td>{{ $product['code'] }}</td>
            <td>{{ $product['name'] }}</td>
            <td>{{ number_format($product->price, 2, ",", ".") }} €</td>
            <td>{{ $product->family->name }}</td>
            <td>
                <form method="post" action="/products/{{ $product->id}}">
                {{ csrf_field() }}
                    <a href="/products/{{ $product['id'] }}/edit">Editar</a>
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Borrar">
                    <a href="/products/{{ $product->id }}">Ver</a>
                </form>
            </td>
        </tr>  
        @endforeach
    </table>
@endsection
