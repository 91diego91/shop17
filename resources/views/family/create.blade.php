@extends('layouts.app')

@section('title', 'create Family')

@section('content')
    <h1>Alta de Familia</h1>
    <form action="/families" method="post">
        {{ csrf_field() }} <!-- Esto es la firma de que es un formulario contruido por laravel, si no tengo esto no me funciona!-->
        <div class="form-group">
            <label>Codigo</label>
            <input type="text" name="code" value="{{ old('code') }}">{{ $errors->first('code') }}<br>
        </div>
        <div class="form-group">
            <label>Nombre</label>
            <input type="text" name="name" value="{{ old('name') }}">{{ $errors->first('name') }}<br>
        </div>
        <div class="form-group">
            <label></label>
            <input type="submit" value="Guardar" class="btn btn-default"><br>
        </div>
    </form>
    <ul>
    
@endsection
