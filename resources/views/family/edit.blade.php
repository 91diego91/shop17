@extends('layouts.app')

@section('title', 'Index Family')

@section('content')
    <h1>Edicion de familia</h1>
    <form action="/families/{{ $family->id }}" method="post">

    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT" ><!--para guardar ya que solo podemos hacer en los formularios get o post, con lo que me intentaria crar uno nuevo!-->

    <div class="form-group">
        <label>Id: </label>
        <input type="text" name="id" value="{{ $family['id'] }}" readonly="readonly">
    </div>
    <div class="form-group">
        <label>Code: </label>
        <input type="text" name="code" value="{{ old('code', $family['code']) }}"><!--para que no pierda los datos de la palabra mal escrita!-->
        {{ $errors->first('code') }}
    </div>
    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ old('name', $family['name']) }}">
    </div>
    <input type="submit" name="Guardar" value="Guardar">
    </form>
@endsection
