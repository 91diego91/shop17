@extends('layouts.app')

@section('content')
    <h1>Familias</h1>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Code</th>
                <th>Name</th>
            </tr>
        </thead>
        <tbody id="tbodyMain">
            <tr><td></td></tr> 
        </tbody>
    </table>
@endsection

@section('scripts')
    <script type="text/javascript" src="/js/family-ajax.js"></script>
@stop
