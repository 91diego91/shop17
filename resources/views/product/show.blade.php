@extends('layouts.app')

@section('content')

    <h1>Detalle de producto</h1>
    <p>ID: {{ $product->id }} </p>
    <p>Código: {{ $product->code }} </p>
    <p>Nombre: {{ $product->name }} </p>
    <p>Precio: {{ $product->price }} </p>
    <p>Familia: {{ $product->family->name }} </p>

@endsection('content')