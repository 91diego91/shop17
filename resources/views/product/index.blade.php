@extends('layouts.app')

@section('title', 'Index Family')

@section('content')
    <h1>Lista de Productos</h1>
    <p><a href="/products/create">Nuevo</a></p>

    <table class="table table-striped">
        <tr>
            <th>Id</th>
            <th>Code</th>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Familia</th>
            <th>Acciones</th>
        </tr>
      @foreach ($products as $product)
        <tr>
            <td>{{ $product['id'] }}</td>
            <td>{{ $product['code'] }}</td>
            <td>{{ $product['name'] }}</td>
            <td>{{ number_format($product->price, 2, ",", ".") }} €</td>
            <td>{{ $product->family->name }}</td>
            <td>
                <form method="post" action="/products/{{ $product->id}}">
                {{ csrf_field() }}
                    <a href="/products/{{ $product['id'] }}/edit">Editar</a>
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Borrar">
                    <a href="/products/{{ $product->id }}">Ver</a>
                </form>
            </td>
        </tr>  
        @endforeach
    </table>
    {!! $products->render() !!}
@endsection
