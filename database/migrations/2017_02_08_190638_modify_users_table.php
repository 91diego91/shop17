<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersTable extends Migration
{
    /**
     * Esto es una migracion que no crea ninguna tabla, modifica una tabla.
     *
     * @return void
     */
    public function up()
    {
        //atento al Schema::table
        Schema::table('users', function (Blueprint $table) {
            $table->integer('role_id')->unsigned()->default(2);//el default para que por defecto sea el rol cliente
            $table->string('surname', 100);
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_role_id_foreign');//tabla en la que estamos_la columna_y el foreign
        });
    }
}
