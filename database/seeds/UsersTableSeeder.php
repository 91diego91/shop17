<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => 1,
            'name' => 'pepe',
            'surname' => 'garcia martinez',
            'email' => 'pepe@gmail.com',
            'password' => bcrypt('pepe'),
        ]);

        DB::table('users')->insert([
            'role_id' => 2,
            'name' => 'juan',
            'surname' => 'yeye martinez',
            'email' => 'juan@gmail.com',
            'password' => bcrypt('juan'),
        ]);

        DB::table('users')->insert([
            'role_id' => 2,
            'name' => 'ana',
            'surname' => 'adad martinez',
            'email' => 'ana@gmail.com',
            'password' => bcrypt('ana'),
        ]);
    }
}
