<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 100; $i++) {
            $order = factory(App\Order::class)->create();//dame un pedido y con el create me lo guardas en bbdd, es un pedido vacio



            $products = App\Product::all()->random(rand(2, 5));//le estoy diciendo que cree una lista de entre 2 y 5 productos
            foreach ($products as $product) {
                $id = $product->id;
                $price = $product->price;
                $quantity = rand(1, 50);
                // $product = App\Product::random(rand(0, 5));
                //estoy añadiendo de los productos que ya existen, NO CREANDO
                $order->products()->attach($id, [
                    'price' => $price,
                    'quantity' => $quantity
                    ]);
            }
        }
    }
}
