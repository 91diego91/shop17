<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'code' => 'rat',
            'name' => 'raton',
            'price' => 10,
            'family_id' => 1,
        ]);//estamos metiendo un array

        factory(App\Product::class, 100)->create();//que utilice el model factory para que cree 100 demanera aleatoria,para crearlo en database, factories
    }
}
