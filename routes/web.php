<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/casa', function () {
        return 'Pagina Home';
})->middleware('admin');

Route::get('families/ajax', 'FamilyController@ajax');
Route::resource('families', 'FamilyController');//ruta especial, implica que
Route::get('families/{id}/products', 'FamilyController@showProducts');//para que nos muestre todos los productos para la familia id que yo pase por argumento

Route::resource('products', 'ProductController');

Route::get('fluent/{method}', 'PruebasController@index');//fluent es el nombre del constructor de consultas para pruebas

Route::resource('orders', 'OrderController');
Route::get('/orders/products/{id}', 'OrderController@addProduct');
